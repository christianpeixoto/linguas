/* global Materialize, Connection */

$(function() {
    app.carregar("principal");

    document.addEventListener("offline", app.showOffline, false);
    document.addEventListener("online", app.showOnline, false);    
    
});

var app = {   
    showAlerta: function(mensagem) {
        Materialize.toast(mensagem, 1000);
        //alert(mensagem);
    },

    showOffline: function() {
        var networkState = navigator.connection.type;

        if (networkState === Connection.NONE) {
            app.showAlerta("Sem conexão");
        }    
    },

    showOnline: function() {
        var networkState = navigator.connection.type;

        if (networkState !== Connection.NONE) {
            app.showAlerta("Conectado");
        }    
    },

    carregar: function(modulo){   
        $("#modulohtml").empty();
        $("#modulojs").empty();

        $("#modulohtml").load( "modules/" + modulo + "/" + modulo + ".html", function(response, status, xhr){       
            if(status === "error"){
                alert("Error: " + xhr.status + ": " + xhr.statusText);
            }else{
                $.getScript( "modules/" + modulo + "/" + modulo + ".js" );
            }
        });    
    },
    
    iniciar: function(){
        $(".button-collapse").sideNav();

        $(".palavrasmagicas").on("click", function(){
            app.carregar("palavrasmagicas");
        });

        $(".cafe").on("click", function(){
            app.carregar("cafe");
        });

        $(".almoco").on("click", function(){
            app.carregar("almoco");
        });

        $(".pizza").on("click", function(){
            app.carregar("pizza");
        });

        $(".farmacia").on("click", function(){
            app.carregar("farmacia");
        });

        $(".loja").on("click", function(){
            app.carregar("loja");
        });

        $(".ondefica").on("click", function(){
            app.carregar("ondefica");
        });

        $("#sidenav-overlay").click();
    }
};